import reader

class Article:
    def __init__(self, number, title, author, b_data, abstract):
        self.number = number
        self.title = title
        self.author = author
        self.b_data = b_data
        self.abstract = abstract.replace("\n", " ")
        self.tokens = self.tokenize()

    def tokenize(self):
        tokens = dict()
        for word in self.abstract.split(" "):
            if word in tokens:
                freq = tokens.get(word)
                updated_freq = freq + 1
                tokens.update({word:updated_freq})
            else:
                tokens.update({word: 1})
        return tokens

    def __str__(self):
        return self.title


class BooleanSearcher:
    def __init__(self, articles):
        self.articles = articles

    def search(self, query):
        query_parser = reader.Parser()

        binary_query = self.binarize_query(query)
        binary_result = query_parser.parse(binary_query)
        self.describe_result(binary_result, query)

    def binarize_query(self, query):
        query_terms = query.split(" ")
        binarized_query = ""

        for i in range(len(query_terms)):
            if query_terms[i] not in ("not", "and", "or"):
                binarized_query = binarized_query + self.get_term_binary(query_terms[i])
            else:
                binarized_query = binarized_query + query_terms[i]
            if i < len(query_terms)-1:
                binarized_query = binarized_query + " "
        return binarized_query

    def get_term_binary(self, term):
        binary = ""
        for article in self.articles:
            if term in article.tokens:
                binary = binary + "1"
            else:
                binary = binary + "0"
        return binary

    def describe_result(self, result, query_tokens):
        print("The result from the query '{}' are:".format(query_tokens))
        for i, bit in zip(range(len(result)), result):
            if bit == "1":
                print(str(i+1), end=" ")


if __name__ == '__main__':
    searcher = BooleanSearcher(reader.Parser().parse_documents("../rsc/cran.all.1400"))
    while 1:
        searcher.search(input("Introduce the query to search. You can use logical operators (and, not, or) withouth parenthesis."))
        if input("\nAgain? Y/N").lower() == "n":
            break
