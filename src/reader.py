from boolean_model import Article


class SyntaxError(Exception):
    pass


class Parser:
    def __init__(self):
        self.last_token = ""

    def parse_documents(filename):
        articles = list()
        with open(filename, "r") as file:
            lines = file.readlines()
            temp_article_data = list()
            last_text = ""
            last_tag = None

            for line in lines:
                if line[:2] in (".T", ".W", ".A", ".B", ".I"):
                    if (last_text != "" and last_tag is None) or (last_text == "" and last_tag is not None):
                        temp_article_data.append(last_text)
                        last_text = ""

                    tag = line[:2]
                    if tag in (".T", ".W", ".A", ".B"):
                        last_tag = tag
                    else:
                        if temp_article_data:
                            to_add = Article(*temp_article_data[:5])
                            articles.append(to_add)
                            temp_article_data = list()
                        last_text = line[2:-1]
                        last_tag = None
                else:
                    last_text = last_text + line
                    last_tag = None
            return articles

    def parse(self, query):
        return self.term(query)

    def term(self, q):
        word = q.split()[0]

        if ("and" in q) or ("or" in q) or ("not" in q):
            if word == "not":
                return self.not_term(q[4:])
            elif word not in ("and", "or"):
                self.last_token = word
                return self.and_or_term(q[len(word)+1:])
            else:
                raise SyntaxError
        else:
            if self.is_terminal(q):
                return q
            else:
                raise SyntaxError

    def and_or_term(self, q):
        operator = q[:3]

        if operator == "and":
            return self.and_operator(q[4:])
        elif operator == "or ":
            return self.or_operator(q[3:])
        else:
            raise SyntaxError

    def or_operator(self, q):
        t1 = self.last_token

        if self.is_terminal(q) and not self.is_operator(q):
            return self.logical_or(t1, q)
        else:
            return self.logical_or(t1, self.term(q))

    def logical_or(self, t1, t2):
        result = ""
        for b1, b2 in zip(t1, t2):
            if b1 == "1" or b2 == "1":
                result = result + "1"
            else:
                result = result + "0"
        return result

    def and_operator(self, q):
        t1 = self.last_token

        if self.is_terminal(q) and not self.is_operator(q):
            return self.logical_and(t1, q)
        else:
            return self.logical_and(t1, self.term(q))

    def logical_and(self, t1, t2):
        result = ""
        for b1, b2 in zip(t1, t2):
            if b1 == "0" or b2 == "0":
                result = result + "0"
            else:
                result = result + "1"
        return result

    def not_term(self, q):
        if self.is_terminal(q):
            return self.logical_not(q)
        else:
            return self.logical_not(self.term(q))

    def is_terminal(self, term):
        if " " not in term:
            return True
        else:
            return False

    def is_operator(self, term):
        if ("not" not in term) or ("and" not in term) or ("or" not in term):
            return False
        return True

    def logical_not(self, binary_term):
        result = ""
        for bit in binary_term:
            if bit == "1":
                result = result + "0"
            else:
                result = result + "1"
        return result


    def parse_documents(self, filename):
        articles = list()
        with open(filename, "r") as file:
            lines = file.readlines()
            temp_article_data = list()
            last_text = ""
            last_tag = None

            for line in lines:
                if line[:2] in (".T", ".W", ".A", ".B", ".I"):
                    if (last_text != "" and last_tag is None) or (last_text == "" and last_tag is not None):
                        temp_article_data.append(last_text)
                        last_text = ""

                    tag = line[:2]
                    if tag in (".T",".W",".A",".B"):
                      last_tag = tag
                    else:
                        if temp_article_data:
                            to_add = Article(*temp_article_data[:5])
                            articles.append(to_add)
                            temp_article_data = list()
                        last_text = line[2:-1]
                        last_tag = None
                else:
                    last_text = last_text + line
                    last_tag = None
            return articles